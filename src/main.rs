use std::{
    collections::HashMap,
    fmt,
    convert::{TryFrom, TryInto},
    borrow::Cow,
    fs::File,
    error::Error,
    io::{self, BufRead, Write},
};
use gif::{
    Frame,
    DisposalMethod,
    Encoder,
};

const DEAD: &str = ".";
const ALIVE: &str = "@";

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
struct Loc {
    x: i64,
    y: i64,
}

#[derive(Clone, Copy, Debug)]
struct Zone {
    ul: Loc, // Upper left
    lr: Loc, // Lower right
}

#[derive(Debug, Clone, Copy)]
enum Cell {
    Dead,
    Alive,
}

#[derive(Clone, Debug)]
struct State {
    alive: HashMap<Loc, ()>,
    size: Zone,
}

#[derive(Clone, Debug)]
struct World {
    name: String,
    state: State,
    size: Zone,
}

macro_rules! world {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_world = World::new();
            $(
                temp_world.insert(Loc::from_tuple($x));
            )*
            temp_world
        }
    };
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Cell::Dead => DEAD,
            Cell::Alive => ALIVE,
        })
    }
}

impl Loc {
    fn neighbors(&self) -> [Loc; 8] {
        [ Loc { x: self.x - 1, y: self.y - 1 }
        , Loc { x: self.x    , y: self.y - 1 }
        , Loc { x: self.x + 1, y: self.y - 1 }
        , Loc { x: self.x + 1, y: self.y     }
        , Loc { x: self.x + 1, y: self.y + 1 }
        , Loc { x: self.x    , y: self.y + 1 }
        , Loc { x: self.x - 1, y: self.y + 1 }
        , Loc { x: self.x - 1, y: self.y     }
        ]
    }

    fn from_tuple((x, y): (i64, i64)) -> Self {
        Loc { x, y }
    }
}

impl State {
    fn new() -> Self {
        Self {
            alive: HashMap::new(),
            size: Zone {
                ul: Loc { x: 0, y: 0 },
                lr: Loc { x: 0, y: 0 },
            },
        }
    }

    fn insert(&mut self, loc: Loc) {
        if loc.x < self.size.ul.x { self.size.ul.x = loc.x; }
        if loc.x > self.size.lr.x { self.size.lr.x = loc.x; }
        if loc.y < self.size.ul.y { self.size.ul.y = loc.y; }
        if loc.y > self.size.lr.y { self.size.lr.y = loc.y; }
        self.alive.insert(loc, ());
    }

    fn remove(&mut self, loc: Loc) {
        self.alive.remove(&loc);
    }

    fn diff(&mut self, other: HashMap<Loc, Cell>) {
        for (loc, cell) in other {
            match cell {
                Cell::Dead => {
                    self.remove(loc);
                },
                Cell::Alive => {
                    self.insert(loc);
                },
            }
        }
    }

    fn to2d(&self, zone: Zone) -> Result<Vec<Vec<Cell>>, Box<dyn Error>> {
        let width: usize = (zone.lr.x - zone.ul.x + 1).try_into()?;
        let height: usize = (zone.lr.y - zone.ul.y + 1).try_into()?;
        let mut strs = vec![vec![Cell::Dead; width]; height];

        for loc in self.alive.keys() {
            let x = loc.x - zone.ul.x;
            let y = loc.y - zone.ul.y;
            if x >= 0 && x <= (zone.lr.x - zone.ul.x)
                && y >= 0 && y <= (zone.lr.y - zone.ul.y) {
                strs[usize::try_from(y)?][usize::try_from(x)?] = Cell::Alive;
            }
        }

        Ok(strs)
    }

    fn show(&self, zone: Zone) -> Result<Vec<String>, Box<dyn Error>> {
        Ok(self.to2d(zone)?.iter().map(|v| // Vec<Cell>
            v.iter().map(|c| // Cell
                (match c {
                    Cell::Dead => DEAD,
                    Cell::Alive => ALIVE,
                }).chars()
            ).flatten().collect::<String>()
        ).collect())
    }

    fn frame(&self, zone: Zone) -> Result<Frame, Box<dyn Error>> {
        macro_rules! m {
            ($f:ident $x:ident $y:ident) => { self.size.$x.$y.$f(zone.$x.$y) }
        }
        let active = Zone {
            ul: Loc { x: m!(max ul x), y: m!(max ul y) },
            lr: Loc { x: m!(min lr x), y: m!(min lr y) },
        };

        Ok(Frame {
            delay: 1, // ?
            dispose: DisposalMethod::Background,
            transparent: None,
            needs_user_input: false,
            top: (active.ul.y - zone.ul.y).try_into()?,
            left: (active.ul.x - zone.ul.x).try_into()?,
            width: (active.lr.x - active.ul.x + 1).try_into()?,
            height: (active.lr.y - active.ul.y + 1).try_into()?,
            interlaced: false,
            palette: None,
            buffer: Cow::from(self.to2d(active)?.iter().flatten().map(|c|
                    *c as u8
            ).collect::<Vec<u8>>()),
        })
    }
}

impl World {
    fn new() -> Self {
        Self {
            name: String::from(""),
            state: State::new(),
            size: Zone {
                ul: Loc { x: 0, y: 0 },
                lr: Loc { x: 0, y: 0 },
            }, 
        }
    }

    fn insert(&mut self, loc: Loc) {
        self.state.insert(loc);
    }

    fn remove(&mut self, loc: Loc) {
        self.state.remove(loc);
    }

    fn diff(&mut self, other: HashMap<Loc, Cell>) {
        self.state.diff(other);
    }

    fn from_rle<I>(lines: I) -> Option<Self>
    where
        I: IntoIterator<Item = String>,
    {
        let mut world = Self::new();
        let mut coords = Loc { x: 0, y: 0 };
        let mut headers = true;
        let mut base_x = 0;

        for l in lines {
            let l = l.trim();
            if headers && l.len() > 1 {
                match l.split_at(2) {
                    ("#N", name) => {
                        world.name = String::from(name.trim_start());
                        continue
                    },
                    ("#R", nums) | ("#P", nums) => {
                        let nums: Vec<i64> = nums.split(" ").filter_map(|n| n.parse().ok()).collect();
                        if nums.len() >= 2 {
                            coords = Loc { x: nums[0], y: nums[1] };
                            base_x = nums[0];
                        }
                        continue
                    },
                    ("#C", _) | ("#c", _) | ("#O", _) | ("#r", _) => continue,
                    ("x ", nums) | ("x=", nums) => {
                        let nums: Vec<i64> = nums.split(|c| c == ' ' || c == ',').filter_map(|n| n.parse().ok()).collect();
                        if nums.len() >= 2 {
                            world.size = Zone {
                                ul: coords,
                                lr: Loc { x: coords.x + nums[0], y: coords.y + nums[1] },
                            };
                        }
                        continue
                    },
                    _ => headers = false
                }
            }

            let mut count = 0;

            for c in l.chars() {
                match c {
                    '$' => {
                        if count == 0 { count = 1 }
                        coords.y += count;
                        coords.x = base_x;
                        count = 0;
                    },
                    'o' => {
                        if count == 0 { count = 1 }
                        while count != 0 {
                            world.insert(coords);
                            coords.x += 1;
                            count -= 1;
                        }
                    },
                    'b' => {
                        if count == 0 { count = 1 }
                        coords.x += count;
                        count = 0;
                    },
                    '0' ..= '9' => {
                        count = count * 10 + i64::from(c.to_digit(10).unwrap());
                    },
                    '!' => return Some(world),
                    _ => {}
                }
            }
        }
        Some(world)
    }
}

impl Iterator for World {
    type Item = State;

    fn next(&mut self) -> Option<Self::Item> {
        let mut diff: HashMap<Loc, Cell> = HashMap::new();
        
        for loc in self.state.alive.keys() {
            let mut neighbors = 0;

            for n in &loc.neighbors() {
                // If neighbor is alive
                if self.state.alive.contains_key(n) {
                    neighbors += 1;
                } else if !diff.contains_key(n) {
                    // We maybe spawn neighbor
                    diff.insert(*n, if n.neighbors().iter().filter(|c|
                            self.state.alive.contains_key(c)).count() == 3
                        { Cell::Alive } else { Cell::Dead });
                }
            }

            // Die if not 2 or 3 neighbors
            if neighbors != 2 && neighbors != 3 {
                diff.insert(*loc, Cell::Dead);
            }
        }

        self.diff(diff);
        Some(self.state.clone())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    /*
    let world = world![
        (0, 0),
        (-1, 0),
        (0, 1),
        (0, -1),
        (1, -1)
    ];
    //println!("{:?}", world.map(|w| w.alive.len()).take(10000).max());
    /*
    println!("{}\n", world.state.show(world.state.size).join("\n"));
    world.take(100).for_each(|w| {
        println!("{}\n", w.show(w.size).join("\n"));
    });
    */
    */
    let stdin = io::stdin();
    let world = World::from_rle(stdin.lock().lines().map(|l| l.unwrap())).unwrap();
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    let color_map = &[0, 0, 0, 0xFF, 0xFF, 0xFF];
    let size = world.size;
    let (width, height) = ((size.lr.x - size.ul.x + 1).try_into()?, (size.lr.y - size.ul.y + 1).try_into()?);
    let mut encoder = Encoder::new(&mut handle, width, height, color_map)?;
    world.take(10000).for_each(|w| {
        encoder.write_frame(&w.frame(size).unwrap()).unwrap();
        //println!("{}\n", w.show(w.size).unwrap().join("\n"));
    });

    Ok(())
}
